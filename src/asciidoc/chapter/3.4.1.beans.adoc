[[beans-factory-scopes-prototype]]
==== The prototype scope
The non-singleton, prototype scope of bean deployment results in the __creation of a new
bean instance__ every time a request for that specific bean is made. That is, the bean
is injected into another bean or you request it through a `getBean()` method call on the
container. As a rule, use the prototype scope for all stateful beans and the singleton
scope for stateless beans.

The following diagram illustrates the Spring prototype scope. __A data access object
(DAO) is not typically configured as a prototype, because a typical DAO does not hold
any conversational state; it was just easier for this author to reuse the core of the
singleton diagram.__

image::images/prototype.png[width=400]

The following example defines a bean as a prototype in XML:

[source,xml,indent=0]
[subs="verbatim,quotes"]
----
	<bean id="accountService" class="com.foo.DefaultAccountService" scope="prototype"/>
----

In contrast to the other scopes, Spring does not manage the complete lifecycle of a
prototype bean: the container instantiates, configures, and otherwise assembles a
prototype object, and hands it to the client, with no further record of that prototype
instance. Thus, although__ initialization__ lifecycle callback methods are called on all
objects regardless of scope, in the case of prototypes, configured __destruction__
lifecycle callbacks are __not__ called. The client code must clean up prototype-scoped
objects and release expensive resources that the prototype bean(s) are holding. To get
the Spring container to release resources held by prototype-scoped beans, try using a
custom <<beans-factory-extension-bpp,bean post-processor>>, which holds a reference to
beans that need to be cleaned up.

In some respects, the Spring container's role in regard to a prototype-scoped bean is a
replacement for the Java `new` operator. All lifecycle management past that point must
be handled by the client. (For details on the lifecycle of a bean in the Spring
container, see <<beans-factory-lifecycle>>.)



[[beans-factory-scopes-sing-prot-interaction]]
==== Singleton beans with prototype-bean dependencies
When you use singleton-scoped beans with dependencies on prototype beans, be aware that
__dependencies are resolved at instantiation time__. Thus if you dependency-inject a
prototype-scoped bean into a singleton-scoped bean, a new prototype bean is instantiated
and then dependency-injected into the singleton bean. The prototype instance is the sole
instance that is ever supplied to the singleton-scoped bean.

However, suppose you want the singleton-scoped bean to acquire a new instance of the
prototype-scoped bean repeatedly at runtime. You cannot dependency-inject a
prototype-scoped bean into your singleton bean, because that injection occurs only
__once__, when the Spring container is instantiating the singleton bean and resolving
and injecting its dependencies. If you need a new instance of a prototype bean at
runtime more than once, see <<beans-factory-method-injection>>



[[beans-factory-scopes-other]]
==== Request, session, and global session scopes
The `request`, `session`, and `global session` scopes are __only__ available if you use
a web-aware Spring `ApplicationContext` implementation (such as
`XmlWebApplicationContext`). If you use these scopes with regular Spring IoC containers
such as the `ClassPathXmlApplicationContext`, you get an `IllegalStateException`
complaining about an unknown bean scope.


[[beans-factory-scopes-other-web-configuration]]
===== Initial web configuration
To support the scoping of beans at the `request`, `session`, and `global session` levels
(web-scoped beans), some minor initial configuration is required before you define your
beans. (This initial setup is __not__ required for the standard scopes, singleton and
prototype.)

How you accomplish this initial setup depends on your particular Servlet environment..

If you access scoped beans within Spring Web MVC, in effect, within a request that is
processed by the Spring `DispatcherServlet`, or `DispatcherPortlet`, then no special
setup is necessary: `DispatcherServlet` and `DispatcherPortlet` already expose all
relevant state.

If you use a Servlet 2.5 web container, with requests processed outside of Spring's
DispatcherServlet (for example, when using JSF or Struts), you need to register the
`org.springframework.web.context.request.RequestContextListener` `ServletRequestListener`.
For Servlet 3.0+, this can done programmatically via the `WebApplicationInitializer`
interface. Alternatively, or for older containers, add the following declaration to
your web application's `web.xml` file:

[source,xml,indent=0]
[subs="verbatim,quotes"]
----
	<web-app>
		...
		<listener>
			<listener-class>
				org.springframework.web.context.request.RequestContextListener
			</listener-class>
		</listener>
		...
	</web-app>
----

Alternatively, if there are issues with your listener setup, consider the provided
`RequestContextFilter`. The filter mapping depends on the surrounding web
application configuration, so you have to change it as appropriate.

[source,xml,indent=0]
[subs="verbatim,quotes"]
----
	<web-app>
		...
		<filter>
			<filter-name>requestContextFilter</filter-name>
			<filter-class>org.springframework.web.filter.RequestContextFilter</filter-class>
		</filter>
		<filter-mapping>
			<filter-name>requestContextFilter</filter-name>
			<url-pattern>/*</url-pattern>
		</filter-mapping>
		...
	</web-app>
----

`DispatcherServlet`, `RequestContextListener` and `RequestContextFilter` all do exactly
the same thing, namely bind the HTTP request object to the `Thread` that is servicing
that request. This makes beans that are request- and session-scoped available further
down the call chain.


